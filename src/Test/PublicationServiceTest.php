<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Service\PublicationService;

class PublicationServiceTest extends TestCase
{

    private $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new PublicationService();
    }


    public function testCreerPublicationUtilisateurInexistant(){
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->service->creerPublication(-1, "uughtfj");
    }

    public function testCreerPublicationVide(){
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->service->creerPublication(1, "");
    }

    public function testCreerPublicationTropGrande(){
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->service->creerPublication(1,  str_repeat("NON ", "100"));
    }

    public function testNombrePublications(){
        self::assertEquals(9, sizeof($this->service->recupererPublications()));
    }

    public function testNombrePublicationsUtilisateur(){
        self::assertEquals(1, sizeof($this->service->recupererPublicationsUtilisateur(1)));
    }

    public function testNombrePublicationsUtilisateurInexistant(){
        self::assertEquals(0, sizeof($this->service->recupererPublicationsUtilisateur(-1)));
    }

}