<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Service\Exception\ServiceException;

interface UtilisateurServiceInterface
{
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil);

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur;

    /**
     * @throws ServiceException
     */
    public function connection($login, $mdp);

    public function deconnecter();
}