<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepository)
    {}
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil) {
        if ($login != null || $motDePasse != null || $email != null || $donneesPhotoDeProfil != null)
        {
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
            }
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException( "Mot de passe invalide!");
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException( "L'adresse mail est incorrecte!");
            }

            $utilisateurRepository = $this->utilisateurRepository;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException( "Ce login est déjà pris!");
            }

            $utilisateur = $utilisateurRepository->recupererParEmail($email);
            if ($utilisateur != null) {
                throw new ServiceException( "Un compte est déjà enregistré avec cette adresse mail!");
            }

            $mdpHache = MotDePasse::hacher($motDePasse);

            // Upload des photos de profil
            // Plus d'informations :
            // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

            // On récupère l'extension du fichier
            $explosion = explode('.', $donneesPhotoDeProfil);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException("La photo de profil n'est pas au bon format!");
            }
            // La photo de profil sera enregistrée avec un nom de fichier aléatoire
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $donneesPhotoDeProfil;
            $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
            move_uploaded_file($from, $to);

            $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);
        } else {
            throw new ServiceException( "Login, nom, prenom ou mot de passe manquant.");
        }
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur
    {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur) ?? null;
     if(!$autoriserNull && $utilisateur == null) {
            throw new ServiceException("Utilisateur inconnu");
        }
     return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function connection($login, $mdp)
    {
        if ($login == null || $mdp == null) {
            throw new ServiceException("Champ manquant");
        }
        $utilisateurRepository = $this->utilisateurRepository;
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($login) ?? null;

        if ($utilisateur == null) {
            throw new ServiceException("Utilisateur inexistant");
        }

        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect");
        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    public function deconnecter(){
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Pas connexter");
        }
        ConnexionUtilisateur::deconnecter();
    }
}