<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurPublication;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class PublicationService implements PublicationServiceInterface
{
    public function __construct(private PublicationRepositoryInterface $interfacePublication, private UtilisateurRepositoryInterface $interfaceUtilisateur){

    }

    /**
     * @throws \Exception
     */
    public function recupererPublications(): array
    {
        return $this->interfacePublication->recuperer();
    }


    public function creerPublication($idUtilisateur, $message)
    {
        $utilisateur = $this->interfaceUtilisateur->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException("Il faut être connecté pour publier un feed");
        }

        if ($message == null || $message == "") {
            throw new ServiceException("Le message ne peut pas être vide!");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Le message ne peut pas dépasser 250 caractères!");
        }
        $publication = Publication::create($message, $utilisateur);
        $this->interfacePublication->ajouter($publication);
    }

    /**
     * @throws \Exception
     */
    public function recupererPublicationsUtilisateur($idUtilisateur): array
    {
        return $this->interfacePublication->recupererParAuteur($idUtilisateur);
    }
}