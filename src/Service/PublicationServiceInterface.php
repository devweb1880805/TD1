<?php

namespace TheFeed\Service;

interface PublicationServiceInterface
{
    public function recupererPublications(): array;

    public function creerPublication($idUtilisateur, $message);

    /**
     * @throws \Exception
     */
    public function recupererPublicationsUtilisateur($idUtilisateur): array;
}