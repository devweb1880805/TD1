<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ControleurUtilisateur extends ControleurGenerique
{
    public function __construct(private UtilisateurServiceInterface $utilisateurService, private PublicationServiceInterface $publicationService)
    {}

    public  function afficherErreur($messageErreur = "", $controleur = ""): \Symfony\Component\HttpFoundation\Response
    {
        return parent::afficherErreur($messageErreur, "afficherListe");
    }

    /**
     * @throws \Exception
     */
    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public  function afficherPublications($idUtilisateur): \Symfony\Component\HttpFoundation\Response
    {
        try {
            $this->utilisateurService->recupererUtilisateurParId($idUtilisateur, false);
        } catch (ServiceException $e) {
            MessageFlash::ajouter('error', $e->getMessage());
            return self::rediriger("afficherListe");
        }
        return $this->afficherTwig('publication/page_perso.html.twig', [
            "publications" => $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur),
            "login" => $this->utilisateurService->recupererParClePrimaire($idUtilisateur)->getLogin()
        ]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    #[Route(path: '/inscription', name:'afficherFormulaireCreation', methods:["GET"])]
    public  function afficherFormulaireCreation(): \Symfony\Component\HttpFoundation\Response
    {
        return $this->afficherTwig("utilisateur/inscription.html.twig");
    }

    #[Route(path: '/inscription', name:'creerDepuisFormulaireInscription', methods:["POST"])]
    public  function creerDepuisFormulaire(): Response
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;

        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        } catch (ServiceException $e) {
            MessageFlash::ajouter('error', $e->getMessage());
            return $this->rediriger("afficherFormulaireCreation");
        }
        MessageFlash::ajouter('success', 'Utilisateur créé');
        return $this->rediriger("afficherFormulaireConnexion");
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public  function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig('utilisateur/connexion.html.twig');
    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public  function connecter(): Response
    {
        try {
            $this->utilisateurService->connection($_POST["login"], $_POST["mot-de-passe"]);
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e);
            return $this->rediriger("afficherFormulaireConnexion");
        }
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public  function deconnecter(): Response
    {
        try{
            $this->utilisateurService->deconnecter();
        }catch (ServiceException $e){
            MessageFlash::ajouter('error', $e);
        }
        return $this->rediriger("afficherListe");
    }
}
