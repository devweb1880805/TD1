<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use TheFeed\Lib\AttributeRouteControllerLoader;

class RouteurURL
{
    public static function traiterRequete()
    {
        $conteneur = new ContainerBuilder();
        $conteneur->setParameter('project_root', __DIR__.'/../..');

        //On indique au FileLocator de chercher à partir du dossier de configuration
        $loader = new YamlFileLoader($conteneur, new FileLocator(__DIR__."/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("conteneur.yml");

        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();
        $routes = (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);

        //$requete = new Request($_GET,$_POST,[],$_COOKIE,$_FILES,$_SERVER);
        $requete = Request::createFromGlobals();
        $contexteRequete = (new RequestContext())->fromRequest($requete);

        //Après l'instanciation de l'objet $contexteRequete
        $conteneur->set('request_context', $contexteRequete);
        //Après que les routes soient récupérées
        $conteneur->set('routes', $routes);

        $generateurUrl = $conteneur->get('url_generator');
        $assistantUrl = $conteneur->get('url_helper');

        $twig = $conteneur->get('twig');

        $twig->addFunction(new TwigFunction("route", [$generateurUrl, "generate"]));
        $twig->addFunction(new TwigFunction("asset", [$assistantUrl, "getAbsoluteUrl"]));

        $user = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $twig->addGlobal('user', $user);

        $twig->addGlobal('messagesFlash', new MessageFlash());

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (MethodNotAllowedException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage(), 405);
        } catch (ResourceNotFoundException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage(), 404);
        } catch (\Exception $exception) {
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage()) ;
        }
        $reponse->send();
        /**
         * NoConfigurationException If no routing configuration could be found
         * ResourceNotFoundException If the resource could not be found
         * MethodNotAllowedException If the resource was found but the request method is not allowed
         * RuntimeException When no value could be provided for a required argument
         * BadRequestException when the request has attribute "_check_controller_is_allowed" set to true and the controller is not allowed
 */
    }
}