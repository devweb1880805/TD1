<?php

namespace TheFeed\Controleur;

use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ControleurPublication extends ControleurGenerique
{
    public function __construct(private PublicationServiceInterface $publicationService)
    {}
    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    #[Route(path: ['/', '/publications'], name:'afficherListe', methods:["GET"])]
    public function afficherListe() : Response
    {
        $publications = $this->publicationService->recupererPublications();
        return $this->afficherTwig('publication/feed.html.twig', [
            "publications" => $publications,
        ]);
    }

    #[Route(path: '/publications', name:'creerDepuisFormulaire', methods:["POST"])]
    public function creerDepuisFormulaire()
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try{
            $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
        }catch (ServiceException $e){
            MessageFlash::ajouter('error', $e->getMessage());
        }
        return $this->rediriger('afficherListe');
    }
}